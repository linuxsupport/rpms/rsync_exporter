%global debug_package %{nil}
Name:           rsync_exporter
Version:        1.0
Release:        1%{?dist}
Summary:        prometheus exporter for rsync

License:        GPLv3
URL:            https://github.com/silvestriluca/rsync-prometheus-exporter
Source0:        %{name}-%{version}.tgz
BuildRequires:  golang

%description
prometheus exporter for rsync

%prep
%autosetup

%build
cd src
go build -v -o ../%{name}

%install
install -Dpm 0755 %{name} %{buildroot}/usr/local/bin/%{name}


%files
/usr/local/bin/%{name}

%changelog
* Mon Nov 20 2023 Ben Morrice <ben.morrice@cern.ch>
- Initial build
